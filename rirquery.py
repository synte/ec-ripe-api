#! /usr/bin/env python3
# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

from args_parser.args_parser import args_parser
from markup_formatter.formatter import format_dict
from requester.requester import request
from resource_validator.validator import validate

if __name__ == '__main__':
    args = vars(args_parser.parse_args())
    if validate(args['resources']):
        for resource in args['resources']:
            data = request(args['query'], resource)
            print(format_dict(data, args['format']))
