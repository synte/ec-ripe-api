# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

from markup_formatter.formatter import format_dict
import unittest


class TestFormatter(unittest.TestCase):

    def setUp(self):
        self.data = {
            'integer': 3,
            'float': 3.14,
            'string': 'Hello',
            'list': [1, 2, 3, 4, 5],
        }

        self.json = '''\
{
    "float": 3.14,
    "integer": 3,
    "list": [
        1,
        2,
        3,
        4,
        5
    ],
    "string": "Hello"
}'''

        self.yaml = '''\
---
float: 3.14
integer: 3
list:
- 1
- 2
- 3
- 4
- 5
string: Hello
'''

        self.xml = '''\
<?xml version="1.0" ?>
<configuration>
    <integer>3</integer>
    <string>Hello</string>
    <float>3.14</float>
    <list>1</list>
    <list>2</list>
    <list>3</list>
    <list>4</list>
    <list>5</list>
</configuration>
'''

    def test_JSON(self):
        self.assertEqual(format_dict(self.data, 'JSON'), self.json)

    def test_YAML(self):
        self.assertEqual(format_dict(self.data, 'YAML'), self.yaml)

    def test_XML(self):
        self.assertEqual(format_dict(self.data, 'XML'), self.xml)


if __name__ == '__main__':
    unittest.main()
