# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

from pywapa.Parser import Parser


def format_dict(data, markup):
    parser = Parser(markup.lower())
    formatted_data = parser.dump(data)

    # PyWaPa's YAML dumper omits YAML's document delimiter. Add it.
    if markup == 'YAML':
        formatted_data = '\n'.join(['---', formatted_data])

    return formatted_data
