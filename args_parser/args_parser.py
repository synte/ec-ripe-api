# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

import argparse

__description = 'Query a RIR. Only the RIPE NCC is currently supported.'

args_parser = argparse.ArgumentParser(description=__description)

args_parser.add_argument(
    '-f', '--format', choices=['JSON', 'YAML', 'XML'],
    default='JSON', help='The desired output format for the query result (Default is JSON)'
)
args_parser.add_argument(
    '-q', '--query', choices=['as-overview', 'geoloc', 'network-info'],
    required=True, help='The desired query operation'
)
args_parser.add_argument(
    'resources', metavar='resource', nargs='+', help='IP address or AS#'
)
