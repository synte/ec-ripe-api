# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

from IPy import IP
import re


ASN = re.compile(r'AS\d+', re.IGNORECASE)


def validate(resources):
    outcome = True

    for resource in resources:
        if ASN.match(resource):
            continue
        else:
            try:
                IP(resource)
            except ValueError as error:
                outcome = False
                print('\t~ ERROR:', error.args[0])

    return outcome
