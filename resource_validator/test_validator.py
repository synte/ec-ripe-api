# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

from resource_validator.validator import validate
import unittest


class TestValidator(unittest.TestCase):

    def test_invalid_ASNs(self):
        resources = ['asn5678']
        self.assertFalse(validate(resources))

    def test_invalid_IPv4(self):
        resources = ['123.456.789.0']
        self.assertFalse(validate(resources))

    def test_invalid_IPv4_mask(self):
        resources = ['123.0/33']
        self.assertFalse(validate(resources))

    def test_invalid_IPv6(self):
        resources = ['abc:def:ghi::']
        self.assertFalse(validate(resources))

    def test_invalid_resource(self):
        resources = ['1x']
        self.assertFalse(validate(resources))

    def test_valid_ASNs(self):
        resources = ['AS1234', 'as5678', 'aS1357', 'As2468']
        self.assertTrue(validate(resources))

    def test_valid_IPs(self):
        resources = ['8.8.8.8', '100/8', '2001:0000::', '12345']
        self.assertTrue(validate(resources))


if __name__ == '__main__':
    unittest.main()
