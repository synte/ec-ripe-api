# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

import requests

URL = 'https://stat.ripe.net/data/{}/data.json'


def request(query, resource):
    response = requests.get(URL.format(query), params={'resource': resource})
    return response.json()
