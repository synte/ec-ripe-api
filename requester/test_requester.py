# coding: utf-8

# Copyright (C) 2017 Jaime Bemarás
# See LICENSE.txt

from requester.requester import request
import unittest


class TestRequest(unittest.TestCase):

    def test_return_type(self):
        response = request('geoloc', '8.8.8.8')
        self.assertIs(type(response), dict)


if __name__ == '__main__':
    unittest.main()
